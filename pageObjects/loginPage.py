import selenium
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC,wait
from selenium.webdriver.support.wait import WebDriverWait
from selenium.common.exceptions import WebDriverException



class LoginPage:

    link_signon_xpath = "//a[contains(text(),'SIGN-ON')]"
    textbox_username_xpath = "//input[@name='userName']"
    textbox_password_xpath = "//input[@name='password']"
    button_submit_xpath = "//input[@name='submit']"
    label_loginsuccess_xpath = "//h3[contains(.,'Login Successfully')]"

    def __init__(self, driver):
        self.driver = driver

    def clickSignonLink(self):
        wait=WebDriverWait(self.driver,30)
        element=wait.until(EC.element_to_be_clickable((By.XPATH,self.link_signon_xpath)))
        element.click()

    def EnterUsername(self,username):
        wait=WebDriverWait(self.driver,30)
        element=wait.until(EC.element_to_be_clickable((By.XPATH,self.textbox_username_xpath)))
        element.send_keys(username)

    def EnterPassword(self,password):
        wait=WebDriverWait(self.driver,30)
        element=wait.until(EC.element_to_be_clickable((By.XPATH,self.textbox_password_xpath)))
        element.send_keys(password)

    def clickSubmitButton(self):
        wait=WebDriverWait(self.driver,30)
        element=wait.until(EC.element_to_be_clickable((By.XPATH,self.button_submit_xpath)))
        element.click()


