from selenium import webdriver
import pytest
from selenium.webdriver import DesiredCapabilities
from selenium.webdriver.chrome.options import Options

@pytest.fixture()
def setup(browser):
    if browser == 'chrome':
        driver = webdriver.Chrome(executable_path=".\\Drivers\\chromedriver.exe")
        print("Launching chrome browser")
    elif browser == 'firefox':
        driver = webdriver.Firefox(
            executable_path=".\\Drivers\\geckodriver.exe")
        print("Launching firefox browser")
    else:
        driver = webdriver.Ie(
            executable_path=".\\Drivers\\IEDriverServer.exe")
        print("Launching IE browser")
    return driver


def pytest_addoption(parser):
    parser.addoption("--browser")


@pytest.fixture()
def browser(request):
    return request.config.getoption("--browser")


###### pytest HTML report##########

# hooks for adding environment info to the HTML report
def pytest_configure(config):
    config._metadata['Project Name'] = 'Guru 99'
    config._metadata['Module Name'] = 'POC'
    config._metadata['Tester Name'] = 'Nkosi'


# hools
@pytest.mark.optionalhook
def pytest_metadata(metadata):
    metadata.pop("JAVA_HOME", None)
    metadata.pop("Plugin", None)
