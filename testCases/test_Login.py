import pytest
from allure_commons.types import AttachmentType
from pageObjects.loginPage import LoginPage
from utilities.readProperties import ReadConfig
from utilities.customLogger import LogGen
import allure

@allure.severity(allure.severity_level.NORMAL)
class Test_001_Login:
    baseURL = ReadConfig.getApplicationURL()
    username = ReadConfig.getUsername()
    password = ReadConfig.getPassword()
    logger = LogGen.loggen()

    # **************************Login to New Tours application******************************
    # fhjfhfhhf
    #hfhfhhfhf
    #Completed
    @pytest.mark.regression
    @pytest.mark.sanity
    @allure.severity(allure.severity_level.CRITICAL)
    def test_login_to_newtours(self, setup) -> None:
        self.logger.info("**********Login Tests**********")
        self.logger.info("**********Verification of Login Test started**********")
        self.driver = setup
        self.driver.get(self.baseURL)
        self.driver.maximize_window()
        self.lp = LoginPage(self.driver)
        self.lp.clickSignonLink()
        self.lp.EnterUsername(self.username)
        self.lp.EnterPassword(self.password)
        self.lp.clickSubmitButton()
        login_confirmation = self.driver.find_element_by_xpath(self.lp.label_loginsuccess_xpath).text
        if login_confirmation == "Login Successfully":
            allure.attach(self.driver.get_screenshot_as_png(), name="login",
                          attachment_type=AttachmentType.PNG)
            self.logger.error("**********Verification Login Test Passed**********")
            self.driver.close()
            assert True
        else:
            allure.attach(self.driver.get_screenshot_as_png(),name="login",
                          attachment_type=AttachmentType.PNG)
            self.logger.critical("**********Verification of login Test Failed**********")
            self.driver.close()
            assert False


