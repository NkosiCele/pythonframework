import logging


class LogGen:

    @staticmethod
    def loggen():
        logging.basicConfig(filename="C://Users//Cele//PycharmProjects//SeleniumPython//Logss//test.log",
                            format='%(asctime)s: %(levelname)s: %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')
        logger = logging.getLogger()
        logger.setLevel(logging.DEBUG)
        return logger
