import configparser

config=configparser.RawConfigParser()
config.read(".\\configurations\\config.ini")

class ReadConfig():
    # Login and Signup Details below
    @staticmethod
    def getApplicationURL():
        baseURL=config.get('common details','baseURL')
        return baseURL

    @staticmethod
    def getUsername():
        username = config.get('login details', 'username')
        return username

    @staticmethod
    def getPassword():
        password = config.get('login details', 'password')
        return password

